from tkinter import *
from tkinter.messagebox import *
from tkinter.filedialog import *
from PIL import Image, ImageTk
import sys
import os
import webbrowser

windows = Tk()

########################################################
#                  CREATTON DE LA FENETRE +            #
#                          TITLE                       #
########################################################


windows.title('Bienvenue sur la TuxNVape') #Nom de programme
windows.geometry('700x400') #Taille de la fenetre
windows.resizable(width=False, height=False) #empêcher de redimensionner le programme


########################################################
#                           MENU                       #
########################################################


def about():
    showerror('A Propos', "Logiciel crée par Minzord pour TuxNVape")
menubar = Menu(windows)

menu1 = Menu(menubar, tearoff=0)
menu1.add_command(label="A propos", command=about)
menubar.add_cascade(label="Aide", menu=menu1)

windows.config(menu=menubar)

########################################################
#                      BIENVENUE                       #
########################################################

canvas = Canvas(windows, width=457, height=139)
canvas.pack()

photo = Image.open("logo.png")

photo.thumbnail((457, 139), Image.BICUBIC)

logo = ImageTk.PhotoImage(photo)
canvas.create_image(0, 0, image=logo, anchor=NW)

label = Label(windows, text="Bienvenue sur la TuxNVape")
label.pack()

########################################################
#                     BOUTON                           #
########################################################

#SITE
def site(event):
    webbrowser.open_new(r"https://www.tuxnvape.fr/")

bouton=Button(windows, text="Site")
bouton.pack(side=LEFT, padx=50)
bouton.bind("<Button-1>", site)

#YOUTUBE
def youtube(event):
    webbrowser.open_new(r"https://www.youtube.com/channel/UCcQC8yDbwwJazXNEhD9Hu8Q")

bouton=Button(windows, text="Youtube")
bouton.pack(side=LEFT, padx=50)
bouton.bind("<Button-1>", youtube)

#DISCORD

def warningvocal():
    if askyesno('Attention', 'Attention Pour Discord vous devez voir un micro et vous présenté en vocal'):
        webbrowser.open_new(r"http://discord.gg/mmjMHry")
    else:
        showerror('Dommage', "Mais si vous changez d'avis n'hésitez pas")

bouton=Button(windows, text="Discord", command=warningvocal)
bouton.pack(side=LEFT, padx=50)

#GOOGLE PLUS
def googleplus(event):
    webbrowser.open_new(r"https://plus.google.com/u/0/communities/114681800792446887851")

bouton=Button(windows, text="Google Plus")
bouton.pack(side=LEFT, padx=50)
bouton.bind("<Button-1>", googleplus)

#TWITTER
def twitter(event):
    webbrowser.open_new(r"https://twitter.com/TuxnVape")

bouton=Button(windows, text="Twitter")
bouton.pack(side=LEFT, padx=50)
bouton.bind("<Button-1>", twitter)

#FACEBOOK
def facebook(event):
    webbrowser.open_new(r"https://www.facebook.com/Tux-N-Vape-157818681707674/")

bouton=Button(windows, text="Facebook")
bouton.pack(side=LEFT, padx=50)
bouton.bind("<Button-1>", facebook)

windows.mainloop()
